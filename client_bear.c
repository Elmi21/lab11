#include "message.h"

//время сна, одна порция
int main(int argc, char * argv[])
{
    int time_bear, portion_bear, sock, bytes_read;
    char message[] = "B\0", buf_out[CONST_SIZE_NUM];
    time_bear = atoi(argv[1]);
    portion_bear = atoi(argv[2]);
    struct sockaddr_in addr;
    sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock < 0) {
        perror("Ошибка при создании сокета!\n");
        exit(1);
    }
    addr.sin_family = AF_INET;
    addr.sin_port = htons(NUM_PORT);
    addr.sin_addr.s_addr = inet_addr(argv[3]);
    if (connect(sock, (struct sockaddr *) &addr, sizeof(addr)) < 0) {
        perror("Клиент: ошибка при установлении соединения!\n");
        exit(1);
    }
    while (1) {
        recv(sock, buf_out, CONST_SIZE_NUM, 0);
         if (strcmp(buf_out, "C\0") != 0) {
            close(sock);
            exit(0);
         }
        printf("Мишка голоден. Он собирается кушать мед в течение %d секунд в количестве %d \n", time_bear, portion_bear);
        sleep(time_bear);
        send(sock, message, CONST_SIZE_NUM, 0);
        printf("Мишка не голоден %d секунд \n", time_bear);
        sleep(time_bear);
    }
}