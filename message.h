#include <sys/socket.h>
#include <netinet/in.h>
#include <pthread.h>
#include <sys/types.h> 
#include <sys/stat.h> 
#include <wait.h> 
#include <fcntl.h> 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <pthread.h>
#include <time.h>
#include <string.h>
#define NUM_PORT 4080
#define CONST_SIZE_NUM 2
#define SIZE_IP_ADDRESS 15
#define NUM_LISTENERS 100

struct info_for_client
{
    int client_sockfd;
    pthread_mutex_t *mutex;
    int *barrel_honey;
    int *flag;
    int portion_bee;
    int portion_bear;
};