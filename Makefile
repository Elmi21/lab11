lab11_4: client_bee client_bear server
client_bee.o: client_bee.c
	gcc -Wall -g -c client_bee.c -std=c99
client_bear.o: client_bear.c
	gcc -Wall -g -c client_bear.c -std=c99	
server.o: server.c
	gcc -Wall -g -c server.c -std=c99
client_bee: client_bee.o
	gcc -o client_bee client_bee.o -pthread
client_bear: client_bear.o
	gcc -o client_bear client_bear.o -pthread
server: server.o
	gcc -o server server.o -pthread
clean:
	rm -rf server.o server client_bear client_bear.o client_bee client_bee.o