#include "message.h"

//номер пчелы, порция пчелы
int main(int argc, char * argv[])
{
    int sock, time_bee, result;
    struct sockaddr_in addr;
    pthread_t thread_id;
    char buf_out[CONST_SIZE_NUM],  i_str[CONST_SIZE_NUM], ip_address[SIZE_IP_ADDRESS];
    int portion_bee;
    strcpy(i_str, argv[1]);
    portion_bee = atoi(argv[2]);
    strcpy(ip_address, argv[3]);
    sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock < 0) {
        perror("Ошибка при создании сокета!\n");
        exit(1);
    }
    int barrel_honey;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(NUM_PORT);
    addr.sin_addr.s_addr = inet_addr(argv[3]);
    printf("номер сокета из пчелы: %d\n", sock);
    if (connect(sock, (struct sockaddr *) &addr, sizeof(addr)) < 0) {
        perror("Клиент: ошибка при установлении соединения!\n");
        exit(1);
    }
    while (1) {
        recv(sock, buf_out, CONST_SIZE_NUM, 0);
         if (strcmp(buf_out, "C\0") != 0) {
            close(sock);
            exit(0);
         }
        time_bee = 4 + rand() % 6;
        printf("Пчела №%s задерживается на %d секунд\n", i_str, time_bee);
        sleep(time_bee);
        printf("Пчела №%s отправила порцию мёда в количестве %d в бочку\n", i_str, portion_bee);
        send(sock, i_str, CONST_SIZE_NUM, 0);
    }
}