#include "message.h"

void *part_server(void* arg)
{

    char buf_out[CONST_SIZE_NUM], buf_in_kill[] = "K\0", buf_in_continue[]="C\0";
    struct info_for_client *data = (struct info_for_client*) arg;
    while (1) {
        printf ("Статус задачи: %d\n", *(data->flag));
        if (*(data->flag) == 1) {
            printf("Покок закончен!\n");
            send(data->client_sockfd, buf_in_kill, CONST_SIZE_NUM, 0);
            //pthread_mutex_unlock(data->mutex);
            pthread_exit(NULL);
        }
        if (*(data->barrel_honey) == 0) {
            printf("Медведь умирает! \n");
            pthread_mutex_lock(data->mutex);
            *(data->flag)=1;
            pthread_mutex_unlock(data->mutex);
            send(data->client_sockfd, buf_in_kill, CONST_SIZE_NUM, 0);
            //pthread_mutex_unlock(data->mutex);
            pthread_exit(NULL);
        }
        send(data->client_sockfd, buf_in_continue, CONST_SIZE_NUM, 0);
        recv(data->client_sockfd, buf_out, sizeof(buf_out), 0);
        printf("Сервер получил строку  %s\n", buf_out);
        if (strcmp(buf_out, "B\0") != 0) {
            printf("В бочку добавилось мёда %d от пчелы %s\n", data->portion_bee, buf_out);
            pthread_mutex_lock(data->mutex);
            *(data->barrel_honey) += data->portion_bee;
            pthread_mutex_unlock(data->mutex);
        } else {
            pthread_mutex_lock(data->mutex);
            *(data->barrel_honey)-=data->portion_bear;
            pthread_mutex_unlock(data->mutex);
            if (*(data->barrel_honey)<=0) {
                pthread_mutex_lock(data->mutex);
                *(data->barrel_honey)=0;
                pthread_mutex_unlock(data->mutex);
                printf("Медведь доел остатки меда!\n");
            }
            else {
                printf("Медведь доел порцию меда!\n");
            }
        }
        printf("На данный момент в бочке %d меда \n", *(data->barrel_honey));
        //sleep(2);
    }

    
}

int main(int argc, char * argv[])
{
    int num_bees, portion_bear, time_bear, result, server_sockfd, client_sockfd, portion_bee, num_threads = 0, num_clients, barrel_honey = 5, flag=0, i=0;
    struct sockaddr_in addr;
    srand(time(NULL));
    char i_str[CONST_SIZE_NUM];
    pthread_mutex_t mutex;
    pthread_mutex_init(&(mutex), NULL);
    num_bees=atoi(argv[1]);
    portion_bee=atoi(argv[2]);
    portion_bear=atoi(argv[3]);
    time_bear=atoi(argv[4]);
    printf("Изначально в бочке с %d мёда\n", barrel_honey);
    pthread_t threads_clients[num_bees + 1];
    struct info_for_client data_client[num_bees + 1];
    num_clients = num_bees + 1;
    server_sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (server_sockfd < 0) {
        perror("Ошибка при создании сокета!\n");
        exit(1);
    }
    addr.sin_family = AF_INET;
    addr.sin_port = htons(NUM_PORT);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    if (bind(server_sockfd, (struct sockaddr *) &addr, sizeof(addr)) < 0) {
        perror("Ошибка при именовании сокета!\n");
        exit(1);
    }
    listen(server_sockfd, num_clients);
    //многоклиентский сервер
    for(int i=0; i<num_clients; i++) {
        client_sockfd = accept(server_sockfd, NULL, NULL);
        if (client_sockfd > 0) {
            data_client[i].client_sockfd = client_sockfd;
            data_client[i].mutex = &mutex; 
            data_client[i].barrel_honey = &barrel_honey; 
            data_client[i].portion_bear = portion_bear;
            data_client[i].portion_bee = portion_bee;
            data_client[i].flag = &flag;
            printf("Из сервера client_sockfd=%d\n", data_client[i].client_sockfd);
            result = pthread_create(&threads_clients[i], NULL, part_server, &data_client[i]);
            if (result != 0) {
                printf("Ошибка в создании потока!\n");
                exit(1);
            }
        }
    }
    printf("num_clients=%d\n", num_clients);
    for (int i = 0; i < num_clients; i++){
        result = pthread_join(threads_clients[i], NULL);
        if (result != 0) 
            printf("Ошибка при завершении потока!\n");
         else 
            printf("Поток №%d завершен!\n", i);
    }
    close(server_sockfd);
    pthread_mutex_destroy(&(mutex));
    return 0;
}